# Optimism Prime Revenue Sharing Model

## Revenue Sharing

### Discussion

Revenue sharing can be done in a various number of way. The most simple and obvious is to vote for OPP/ETH, it shares revenue to farmers as VELO emissions. This is however highly inefficient for us because:
- Emission APR based on veVELO is 75.6%
  - This can be obtained by dividing annualized weekly emissions to farm (https://optimistic.etherscan.io/token/0x3c8b650257cfb5f272f799f5e2b4e65093a11a05?a=0x3460dc71a8863710d1c907b8d9d5dbc053a4102d) by amount lock
  - Example with current data: 52 * 10,034,576.378545207709075066 / 689,324,071.991283636106675109 = 0.756970477147284
- Most of the emissions are diluted to farmers because we don't own enough POL

I think this strategy could be applied if we choose to stop all developments related to the project. OPP would just be a farming meme token. It would ensure a good level of liquidity for a time. An additional benefit is that it can make Tarot market efficient again for OPP lenders, rewarding holders. However after a time our voting power would be diluted, treasury would not have grown enough, and the price of OPP would suffer. It can probably lasts for years though.

### Proposed Model

A better model would ensure:
- Growth of the treasury
  - currently covered from bribe chasing
- Reward long term participants of the project
- Recover liquidity to a controlled amount
- Ensure liquidity rewards ends up in treasury and not in farmers pockets
  - can be achieved by buying POL (which is currently done with Lenderbot DAO funds, in a DCAed way), but it's ineficient at current PER ratio, and lead to high price impact while liquidity remains low.

I propose to address these needs with a product named maOPP (temporary name, can be customed to use Transformers lore). This product is inspired by maBEETS from Beethoven, suggested by @Gattordo on discord as a good alternative to ve locked models. For more information about maBEETS, please read https://docs.beets.fi/beets/mabeets

A maOPP position would be created as a NFT holding assets. Initially accepted assets would be:
- vAMM-OPP-ETH
- vAMM-OPP-OP
- vAMM-OPP-fBOMB
- vAMM-OPP-VELO
- Tarot lended OPP (simulate single staking with interest rate)

When a user deposit a LP token, it is staked on Velodrome and all VELO rewards accrue to the treasury. Each farmed VELO is locked in a dedicated new veNFT (to be created) of the treasury, and the maOPP NFT responsible for the farming of this VELO accrue an oppVELO, that can be claimed by the holder.

Each maOPP has an OPP score, computed as number of underlying OPP, with a x2 factor for LP positions. A single NFT can hold several instruments, so the score is obtained as a sum of all scores.

The score is then scaled by a maturity factor between 0 and 1, evolving on a maturity curve rewarding long term holders. The curve is to be determined, but will probably be the same as the maBEETs curve if it seems to work well.

A maOPP NFT doesn't lock underlying positions. However, unstaking any amount leads to a reset of the maturity curve. So instead of forcing lock, we punish unstake.

The user can choose to sell the maOPP NFT OTC or on a NFT market.

The scaled OPP score is then used to compute how much revenue share, from the global revenue sharing pool, the NFT will receive on a given epoch.

### oppVELO

oppVELO will act as a VELO derivative, than can be used to vote on specific OPP related farms on Velodrome. Additionally, oppVELO can be staked so the voting power is delegated to treasury managers of Optimism Prime. It can then be used to grow the treasury, control liquidity is a more managed way, etc.

oppVELO gives farmers an opportunity to keep a part of the VELO farmed with their position, but the voting power remains strongly linked to the project and will be used to keep growing our ecosystem.

The rebase earned by VELO locked behind oppVELO will be kept by the treasury, new oppVELO will be minted for each rebase, but kept in treasury and used by treasury managers like they think it's best.

It's not planned in the short term to incentivize oppVELO liquidity, but might become a project if it makes sense. We'll ask for oppVELO listing on Velodrome though.

### Revenue sharing pool

The amount of our veVELO we'll allocate to revenue sharing is yet to be determined, but maybe we can start with the remaining 62.5% of our second veNFT that are not allocated to autobot revenue sharing.

It is 154,661.83125 veVELO = $21652.656375 at 0.14 / VELO. At 60% APR it is approx $250 / week (13K / year). See Data analysis section at the end of the document to understand the 60% APR.

Assuming people are happy with a real yield of 35%, it means we could attract 37K liquidity. If people are happy with 15K, we could attract 87K liquidity. It really depends on our holders mindset.

Based on numbers, we can allocate more votes to the revenue sharing pool.

### Voting power

maOPP score will also give voting power for treasury decision making. That voting power could be fully delegated to treasury managers, but can give large holders more control over Optimism Prime spending decision. We can also build a more general delegation system, to delegate to anyone.

An example of important decision making is the use of our first veNFT voting power. maOPP voting power could be used to choose how this NFT should be used to generate yield, or incentivize our liquidity

maOPP voting power could also be used to choose how much to pay contributors, how to diversify accross assets and investment instruments, what products to build and how much is allocated to their funding, etc.

### Reduce impermanent loss

Most long term holders hate being LP because of impermanent loss. That's why many projects use 80-20 LPs for revenue sharing instead of 50-50.

In our case, most of our revenue comes from Velodrome. Maybe we'll deploy on Beethoven later, but for now we should focus on where we have our voting power.

To avoid IL, a user can stake Tarot's lended OPP as deposit. It can also mix with an LP position to simulate a 80-20 deposit. The system is actually more flexible than a single 80-20 LP token, as a user can do any mixture of OPP LP tokens + single staking, and split accross any number of NFTs.

If we choose to deploy on Beethoven we'll add corresponding LP tokens as stakable assets in maOPP NFTs.

### Usage of Tarot lended OPP

It may happen most users choose to enter maOPP with single staking, which would not increase overall OPP liquidity. In that case, we could profit from that as treasury by depositing our OPP-ETH LP on Tarot, borrow OPP, and yield farm with leverage. We can allocate part of our voting power to the farm so it allows us to build quicker more POL.

Our leverage should be carefully monitored of course, and we'll keep aside enough OPP to quickly repay if necessary. We'll probably automate this strategy if it is deployed.

### Possible extensions

Here are some ideas that could benefit the whole ecosystem.

#### Autobots staking

Any Autobot NFT could be spent to offset the maturity curve by one week. For example, if full maturity is reached after 24 weeks, staking 24 autobots on a maOPP NFT would reach full maturity instantly.

The treasury would keep spent NFTs for later re-use.

#### Pay to mature

A fee curve could be set to buy weeks of maturity. It would gives the treasury a new source of revenue.

#### Name

CBT, for CyBerTron ? (beware of copyright though, maybe find a name inspired)

## Data Analysis

I like to start with some data.

- OPP supply: 970577639.2688208
  - Note: ~30M was "burnt" by sending them to Vitalik: https://optimistic.etherscan.io/token/0x676f784d19c7f1ac6c6beaeaac78b02a73427852?a=0xd8da6bf26964af9d7eed9e03e53415d37aa96045

- OPP price: ~0.0002 USD / OPP
  - Mcap: ~200K

- Liquidity pools:
  - OPP/WETH: $9800 (24,569,403 OPP => 2.45% of supply)
    - Treasury owns $5500 => 56% of LP
  - OPP/OP: $6400 (15,983,792 OPP => 1.59% of supply)
  - OPP/fBOMB: $24000 (60,387,812 OPP => 6.03% of supply)
    - Treasury owns $3955 => 16.4% of LP

- Treasury veVELO:
  - 233,084.37 veVELO
    - Fully owned
  - 247,458.93 veVELO
    - 37.5% reserved to Autobots rewards (92,797.09875)
    - 154,661.83125 for treasury
  - => total veVELO earning for treasury is ~387K veVELO => $54K at $0.14 / VELO

- Current treasury earning potential
  - https://dune.com/0xkhmer/velodrome-historical-voting-apr
  - By voting for WETH/USDC
    - Good fees APR earning real yield in WETH and USDC
    - Correct bribe APR earning OP
    - Fee & bribe APR often above 100% before VELO pump, now above 60%
    - Taking an average of 20% APR on WETH/USDC swap fees it gives us:
      - $54K * 0.2 / 52 = $207 of real yield per week
    - Taking an average of 40% APR on WETH/USDC bribe it gives us:
      - $54K * 0.4 / 52 = $414 of OP yield per week
    - Total is $614 per week
  - Dashboard reports $148 rewards / 100K veVELO on average for Epoch 39 (23 Feb 2023)
    - => 148 * 3.87 = $572 / week for treasury

![](./vAMM-WETH-USDC.PNG)

- PER analysis
  - Warning: we don't know how our revenue will evolve as it depends strongly on Velodrome performance
    - Bullish thesis: Velodrome remains a leading DEX, volume keeps increasing in 2023, Velodrome delivers its V2 with strong inovations.
    - Neutral thesis: volume remains constant but Velodrome keeps its place, it delivers V2
    - Bearish thesis: Optimism as a chain struggles, the Base announcement was just an excuse to attract exit liquidity for OP holdings of Coinbase, Velodrome struggles, Volume decrease, VELO dumps as a consequence
    - Ultra bearish thesis: Bearish thesis + Velodrome enters a death spiral (VELO dumps => APR dumps => TVL dumps => Volume & swap fees dumps => VELO dumps => repeat)
  - Assuming neutrality: $614 per week
    - => $31928 / year
    - => 15.9% of mcap
    - => 6.26 PER ratio (6.26 years of earning to reach entry price)
  - Note: OPP is not competitive as a VELO derivative because:
    - Current VELO locked is 689M => $96.5M mcap at 0.14
    - Current veVELO earning per 100K VELO is $148 => $1.02M per week => 53.04M / year
    - 96.5 / 53.04 = 0.549 => 1.82 PER ratio (1.82 years of earning to reach entry price)
    - Consequence: Optimism Prime needs to increase its revenue, or have a lower price, to increase its PER
