# Treasury

This document discuss strategies to grow the Optimism Prime treasury in order to achieve the vision of the project and to reward token holders and early investors for their engagement.

## Goals & Targets

The treasury of Optimism Prime has several goals and targets depending on time horizon.

**Short term goals (Q1 2023)**

- Acquire a veVELO position large enough to incentivize a given level of liquidity (to be specified) at a given target APR.
- Acquire protocol owned liquidity (POL) OPP/ETH in order to farm our own pool
- Use VELO rewards to:
  - Increase our governance on Velodrome (matching first goal)
  - Pay loans interest (see next section)
  - Increase our OPP/ETH POL by compounding rewards

**Medium term goals (Q2-Q3 2023)**

- Keep growing veVELO position according to market cap growth of OPP
- Start acquiring OP governance to have a stronger position on our ecosystem
- Reach a treasury size allowing to extract a cash flow to pay contributors
- Have at least one working product (DeFi or NFT or both) branded with the project's thematics and extracting fees from users to grow the treasury
- Work to acquire grants from other Optimism projects

**Long term goals (Q4 2023 - 2024)**

- Have a treasury size large enough to enable revenue sharing with stakers
- Diversify the treasury accross a range of assets / protocols to multiply revenue sources and derisk
- Have a complete suite of products branded with the project's thematics ready to capture new entrants of the next bull market
- Send OPP to the moon, achieve a market cap higher that 5M

## Early Growth Ideas

We start from a ~450K veVELO position and ~13M OPP tokens. We currently have no POL so we need find ways to acquire that. OPP is fully distributed, hence we cannot print tokens to raise funds. We have to be creative to overcome this challenge and push the project further.

I describe here several ideas to grow the treasury early on, using funds from investors willing to help the project. For these ideas, I try to match the following quality standards:

- The project should not dump directly OPP tokens. It is better to use OPP tokens in a way that gives the market opportunities, while helping the project grow its treasury.
- The project should compensate early investors with real yield. The initial launch of OPP has been fair, with the idea of having a fix supply token with no team allocation. We don't want to create an additional worthless token that would be used just for fundraising. It would just dilute the potential value of the project in another instrument. It does not mean we won't launch new tokens in the future, but they need to make sense and be tied to specific products.

### Uncollaterized loan of stablecoins

While loan is DeFi is most often done with a collateral, it does not mean we cannot do uncollaterized loan with a fix interest rate. The idea would be to borrow stablecoins from people that want to help the project at an interest rate higher than standard stablecoin yields. The highest dollars peggued stablecoin yield I can find on Velodrome is currently USD+/LUSD at 14.42%. Note that USD+ is also yield bearing, but it bears the risk of an issue with the protocol. sUSD/LUSD is 13.40%, where both expose liquidity risks as their supply is limited by their respective collat ratio. Maybe what could be considered the safer is USDC/DAI, at 4.59%.

Our projects would offer to borrow stablecoin at 24% APR to be competitive to these options. The stablecoin would mainly be used to buy OPP/ETH and farm with it. On a monthly basis, the project would repay only interest rate to lenders, so 2% per month. Assuming the project borrow 10K, the treasury would have to pay 200$ per month to lenders. the project can choose to repay the principal or part of the principal when it wants, but before a maximal period of 5 years.

Risks for lenders:

- If the project does not succeed, it might never be able to repay the principal
- If the project has difficulties with its revenue sources (Velodrome yield, products), it may not be able to pay interest rate some months

Current APR of OPP/ETH is 100%, as long as it remains high enough the project should be able to pay interest. Moreover, accumulating veVELO from the rewards would allow us to increase our own APR, which would benefit the project, individual farmers, and holders.

To ease the investment process, when can setup a new DAO on https://www.xdao.app/ that could be called "OPP Earlybots" and dedicated to this stablecoin loan. For treasury would directly repay to this DAO, and can even repay as a yield bearing asset (stablecoin deposited on AAVE) so investors can keep earning more yield on top of paid interest. The DAO itself can choose when to redistribute to its members using XDAO voting facilities.

About the stablecoin used, USDC would be ideal for liquidity and safety, but we could also go for sUSD (Synthetix USD). Going for sUSD can be risky (if the protocol has issues, or liquidity gets low), but can also be some kind of political choice. By favoring Synthetix stablecoin, we demonstrate our will of using Optimism native protocols and contribute to the ecosystem. Also sUSD has often higher yield on AAVE so interest repaid to the DAO would earn more yield while it sits idle in DAO wallet.

Finally, people that contribute to this strategy would be remembered in the future of the project. If we have ways to give them additional benefits in a fair way, we'll use them.

### Uncollaterized loan of OPP

I won't describe too much this one as the setup would be the same as the previous section, except we would borrow OPP to holders. The interest rate would be lower, for example 12%, 1% per month, because OPP price is very volatile. It can dump and it would easier for the project to pay interet, or it can pump and it would become harder. The principal would be repaid faster, before one year.

The target investor is the one that already accumulated OPP, don't want to expose himself to impermanent loss, wants to help the project's growth, and earn a correct interest rate on it. The risks are the same for the lenders, but there's also opportunity risk: Tarot single staking APR might become higher than 12%. I expect that if this happen, the project health would have been increased and we could repay the loan faster so people can enjoy degen APRs.

The usage of these OPP could be:

- Pair them with ETH bought from the stablecoin loan. Would allow us to start with a higher POL position.
- Use them for bribing if it makes sense (would need APR computation to know the exact amount to use so that our VELO rewards have higher value than OPP used for bribing)
- Use them for bonding, see next section

### Bonding OPP with Bond protocol

I first need to read the documentation of https://docs.bondprotocol.finance/, but the idea would be to offer some OPP for OPP/ETH LP tokens in a bonding process. There are several advantages in doing that instead of selling them:

- First, we don't dump by doing that. We don't hurt our holders.
- We give market some arbitrage opportunities by exposing themselve to the vesting period.
- We acquire OPP/ETH positions that holders of these positions may give up to get more OPP. It means our share in the total LP would be higher, so we would get more share of rewards from farming.

Also it gives us an opportunity of testing the Bond protocol that we could use later for other purpose. It would like to start with a simple test, providing 2M OPP tokens from the ~13M we will get back.

## Early Products Ideas

Here are two ideas of products not to hard to develop, that would provide value to OPP holders as well as fees for the growth of the treasury.

### Simple Autocompounder

Currently, an OPP/ETH staker that wants autocompound can use the Tarot pool. Tarot is usually used for leveraged farming, but a position with 0 leverage results in simple autocompounding.

While great for partnership, our project's does not get any fees from that product. An autocompounder is relatively easy to implement and can be automated with Gelato, requiring no central infrastructure. On top of standard auto-compounding, we can also provide our users more options to compound their rewards. They can choose to autocompound as OPP/ETH, OPP, ETH, keep VELO, or a mix of all of these. We could even add an additional layer of where their rewards could be routed to Tarot into single staking lending on the best performing pool.

Revenue sources:

- Users would deposit OPP/ETH and we could take a deposit fee on that, for example 0.5%.
- We could provide swap facilities, allowing a user to enter with any asset that would be swapped and deposited for OPP/ETH. We could take a swap fee on that, for example 0.1%.
- A 10% performance fee could be taken, which is the same as Tarot. If the product does not attract enough users, we can reduce that fee to be more competitive, but I think that the additional features we would provide could justify taking the same fees. Also it directly helps the project, which is in the interest of all OPP/ETH farmers.

### Fair Lottery

The concept of fair lottery, also known as ["No Loss Prize Savings" (NLPS) pools](https://medium.com/pooltogether/the-power-of-no-loss-prize-savings-1f006503f64) is the one implemented by PoolTogether. I already asked them if they have a permissionless way of deploying custom pools on their protocol. Turns out they are currently developping that and will keep us informed.

In the short term, we could easily implement one allowing us to take some fees from users, while providing them an alternative way to farm OPP/ETH.

The idea of NLPS is quite simple: users deposit OPP/ETH LP tokens, that are used by the protocol for farming during a specific period. At the end of the period, one or several depositors are randomly drawn (where probability depends on their deposit size) and they share a large portion of all rewards. It is a very good product for small farmers because it transforms a constant stream of dusty rewards, that need to be claimed and dumped, into a chance of earning a large quantity of rewards. Variance of the profit is higher, but the expected value is the same.

We could propose a product that split rewards in the following way:

- 10% is autocompounded in OPP/ETH, so everyone always win a bit
- 10% is kept as performance fees
- 80% is split between N winners (to be defined, should probably depends on the number of depositors)

Similar to the simple autocompounder, we could provider options to the user on how to handle the rewards when she wins (compound, swap to OPP or ETH, deposit on Tarot, ...).
