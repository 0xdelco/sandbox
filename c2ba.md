# c2ba

This document is a quick introduction of myself and why I choose to contribute to Optimism Prime.

Most blockchain projects have anonymous teams, which I can understand for their safety and the will of having to lead identity. But it prevents users / retail investors / community members from having a clear idea of their background. Without this information, it's very hard to trust teams behind projects.

In my case I'm not anonymous, you can easily find information about me online, and so I can provide more details about my past and current work experiences, and my motivations for Optimism Prime.

## Context

- I'm a developer and DeFi enthusiast
- I choose to took the "lead" of Optimism Prime after the old team's withdrawal, but I mainly consider myself as an active contributor - I currenly have ownership over infrastructure elements of the project that are hard to decentralize
  - twitter account
  - medium account
  - optimismprime.io domain name
  - discord server
- I am multi-signer on the treasury multi-sig of the project https://app.safe.global/oeth:0x1481BffbB3a825AC994DBdDCEf1eC2082fE35a3D/home with:
  - fierydev from MCLB, also active contributor of Optimism Prime operations
  - RogueAD from Otterclam, also active contributor of Optimism Prime operations
  - GuyWithKeyboard from Velodrome

## Why contributing to Optimism Prime

- I want it to be clear that my contribution is mainly driven by self-interest and not out of charity
- I own a descent bag of OPP (~5% of supply) and I trust my skills to pump that bag from my contributions
- I use Optimism Prime as a project to improve my skills in the blockchain dev space accross several horizontals (code, finance, governance, marketing), and I know I'll learn more from that kind of niche project than from a VC funded project with a large team
- I like the Transformers theme and meme coin
- I like the Optimism chain for it's DeFi core aspect + community and open source driven initiatives
- Building on something that already exist and have a small community is easier that starting something from scratch

## What I want to work on for Optimism Prime

- I'm working on several different projects, so I try to link them to improve my productivity
- It means I will always choose to develop things for OPP that I like and that are useful to me on other projects
- My main drivers are (because useful to me accross several projects):
  - DeFi and algorithmic trading related stuff
  - Financial strategies for treasury allocation
  - Production of educational materials
  - Tooling for blockchain dev
  - Collaboration with other projects
- My secondary drivers are (because fun to me):
  - DeFi gamification, memification, metaversation
  - Game dev, GameFi, Game tokenomics & NFTs
  - Decentralized governance structures
  - Revenue sharing mechanics & decentralized funding

## What I don't want to work on for Optimism Prime

While I aknowledge the importance of the following items for a Meme project like OPP, that won't be something I plan to do myself:

- Aggressive marketing on retails through social networks
- Production of meme materials
- Active community management

I expect these to be addressed at some point by community members.

## Past experience

- I did a PhD in computer graphics, focused on physically based rendering, from 2012 to 2015.
  - You can the thesis here https://theses.hal.science/tel-01320130/
  - And a list of my publication here http://igm.univ-mlv.fr/~lnoel/index.php?section=research#item2
- I worked at Ubisoft Animation Studio (part of Ubisoft Film & Television https://www.ubisoft.com/en-us/entertainment/film-tv) from 2015 to 2020
  - Developer on Shining, an in-house renderer used to render images of the Rabbids Invasion TV series
    - https://www.ubisoft.com/en-us/entertainment/film-tv/rabbids-invasion-series
    - https://gitlab.com/groups/ubisoft-animation-studio/shining/-/archived
  - Developer on Mixer, an addon for real-time collaboration in Blender
    - https://github.com/ubisoft/mixer
- I was freelance developer for Engie from 2020 to early 2022, doing fullstack cloud app development and devops.

## Current experience and relationship to cryptocurrency

- I'm in the crypto market since end of 2017, but has been mainly a hobby for several years
  - was interested in algorithmic trading from the beginning, but mostly implementing small experiments from 2017 to 2020, nothing serious
  - discovered Ethereum, dApps and Solidity in early 2018, but stopped digging in 2019 and only really started learning blockchain dev in 2021
  - missed the opportunity of discovering serious DeFi projects in their early stage
- I'm basically fulltime crypto since early 2021 when I discovered decentralized finance, which has been for me the real trigger
  - A bit mad I did not experienced DeFi summer
  - In 2021 mostly learnt on personal time, had fun and financial losses. Learnt a lot from doing and digging.
- I quit my freelance dev job in 2022 to focus on my own projects, and have at least until April 2024 full time to allocate to them
- My current projects are:
  - Optimism Prime
  - Working on fund raising for a company I will make with a friend
    - We will provide paid online services for retails and companies that want to invest in DeFi
    - More details soon, when deck and whitepaper are ready
  - I'm teaching maths and solidity dev at https://ecole2600.com/, a french cybersecurity school
  - I'm doing R&D on algorithmic trading
  - I'm allocating my own portfolio and experimenting strategies on it
  - I'm tech watching many projects, tokenomics, protocols and their discord activity
  - I'm working with ChainGuardian DAO to try to bring back liquidity to MIX, a token they released in 2021
    - More details here https://dao.chainguardians.io/proposals/0x1d2ca87c0d38510c2de62ef46f66b705e647caf98d0eb4e7fa9f89edd76d06db
- So given I'm invested (timely and financially) in several projects, I will focus for OPP on things that can also bring value to my other projects, and to myself (trying to maximize what I can learn from the experience).
