# Optimism Prime - Vision and Roadmap

Author: c2ba

This document gives a global vision for Optimism Prime, and an attempt roadmap for 2023. At the time of writing (Feb 2023), workforce is quite low so it is hard to expect too many things unless many contributors appear.

I'm presenting here a vision with several ideas arranged in verticals that I believe can fit the project well. It does not mean all of these will be addressed, but give an view of what could be done if we find means. Feel free to join on discord to discuss the vision and add items to the list.

## Vision and verticals for the project

Optimism Prime has been introduced as a MemeFi project, and the goal is to keep developing the project in the Meme + DeFi direction. The memic aspect would remain centered on transformers, and more generally mechas, futuristic and AI stuff. The DeFi aspect would be focused on useful products that could benefit the ecosystem, gamified finance, and partnerships with other DeFi projects.

As with any blockchain project, many horizontals span these verticals and require a wide range of skills:
- Product management
- Development (architecture, frontend, backend, blockchain)
- Design
- Content production (pictures, videos, education materials, infographics, social content)
- Community management
- Finance

Accross all verticals, we'll have to focus on things that can be addressed by current contributors skills.

### DeFi products vertical

- Autocompounding vaults: Prime Vaults
  - Note: Currently being developped for Velodrome farms
- Yield bearing tokens
  - ETH-prime, USD-prime
  - Build on autocompounding vaults of stablepools
  - Could be enhanced with more advanced strategies like leveraged yield farming, delta neutral positions, perpertuals
- Automated trading vaults
  - Trading strategies executed onchain, orchestrated by offchain trading bots
  - R&D on algorithmic trading
  - Enhanced with DeFi product to avoid idle funds: farming, lending, hedging
  - Focus on simple strategies offering exposition and yield with volatility trading
  - More advanced strategies based on trading methodologies and multi-resolution indicators
- Liquidity optimization systems
  - R&D: how to distribute liquidity optimally accross DeFi products
  - Limit orders without idle funds

### Treasury allocation vertical

- Growing the treasury
  - Should be able to pay for contribs
  - Should be healthy, sustainable and diversified
  - Should lead to revenue sharing (R&D: a new market-driven model for revenue sharing and development funding)
- Optimism Maxi
  - Collect governance and yield tokens of the ecosystem (OP, SNX, LYRA, KWENTA, ...)
- Invade other chains
- Go for a new NFT fundraising campaign
  - Only after shipping of some products, and with a purpose (what do we want to do with the funds ?)
  - Higher price than previous one, with real benefits for minters / holders
  - High quality designs
  - Take inspiration from revenue sharing opportunities that Thena and Mummy implemented for their NFT fundraising
  - Offer whitelist and discount to holders of Autobots NFTs
    - Burn the old one to mint a new one
    - Exclusive rarity trait + increased benefits

### Social, Marketing & MemeFi vertical

- Core product: Optimism Prime Park
  - A gamified hub frontend on Optimism where mechas meet DeFi
  - Presented as an entairtainment park with many spots featuring DeFi products
  - A wide range of spot types:
    - Memified frontends, built on top of existing DeFi products
    - Optimism Prime products: out own in-house built products
    - Games in the OPP universe
    - Educative spots: blog articles, documentations, interactive tutorials, ...
    - Social spots: twitter, discord, snapshot voting, crew3 integrations
    - Analytical dashboards
  - Act as a kind of Metaverse for projects on the Optimism chain
  - Marketing idea
    - Spots can be rented to other projects to bring revenue
    - OPP can be used as governance token to choose renting allocations
    - Requirement: Optimism Prime needs to become a well-known project with a large audience for this to make sense
- Social activity
  - Active management of social networks: Twitter, Lens, Discord, ...
  - Content production related to Optimism Prime
    - Meme pictures, stories, infographics, videos, sound, ...
  - Crew3 (and similar platforms) questing
    - Production of fun quests
    - How to reward them ?
  - Wallet Tracker integrations
    - Zapper
    - Debank
    
### GameFi & NFTs vertical

- Give a purpose to Autobots collection
  - Series of mini-games
  - Customization
  - Advantages in the OPP ecosystem
- Re-design, using AI generation
- Gamification of Optimism apps

## Roadmap 2023

The roadmap I give is driven by my personal motivations, what interest me and what can be useful to me on other projects I'm working on. If new people with complementary skills join the project, we'll update the roadmap to include what they want to work on.

For 2023 I will mostly focus on: DeFi development and tooling, automated trading strategies, treasury management, education materials.
  - DeFi and automated trading is what I like the most, it can bring revenue to the project, the things I dev are for my own portfolio too and can be used on another project I have.
  - Treasury management is also something I like, related to DeFi, and is currently our only revenue source. It's basically what's back OPP price while nothing new has been released.
  - Education materials is something I care about and that I need to write for courses I give anyway. Branding them OPP can bring positive awereness to the project, and give us good reputation of knowledge sharers.

- Q1
  - Treasury
    - Investment of Lenderbots DAO funds, evaluation of interest rate to pay
    - Partnership with fBOMB for a fBOMB/OPP farm
    - Acquisition of OPP protocol owned liquidity (OPP/ETH and fBOMB/OPP)
    - Bribe chasing with our veVELO NFTs
  - Dev
    - Release of Prime autocompounding vaults with competitive performance fee, to bring revenue to treasury
    - Overhaul of the frontend (roadmap and vision update, removal of copyrighted picture, colors and logo, removal of staking page, add prime vaults UI)
  - Social
    - Regularly tweet pictures generated by DeFiLogic with AI
- Q2
  - Treasury
    - Re-assert Velodrome positioning, and if we should transition to voting for our pool for POL farming
  - Dev
    - V0 for ETH-prime yield bearing ETH derivative token
    - Work on Automated trading vaults
    - Prototype Optimism Prime Park frontend
  - Education
    - Write Solidity & DeFi tutorials branded OPP
- Q3
  - Treasury
    - Submit proposals for grants, based on what we released in Q1-Q2
  - Dev
    - R&D on Automated trading
    - Polish Optimism Prime Park, bring it forward for proposal for grants
- Q4
  - Finish all things started and unfinished
  - Reflect on what was done, and what to do next in 2024
