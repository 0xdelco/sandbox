# Optimism Prime - Sandbox

This repository is for anyone wanting to contribute to Optimism Prime project.
We use it as a place to draft documents, specifications, ideas and collaborate quickly.
It can also be used to share code experiments that could lead to future products for the project.

Feel free to create issues to dicuss ideas, or submit pull requests to directly contribute on documents / code.

Once something is mature enough (code or documentation), it will be moved to a dedicated repository for production work on it and deployment.

Join us on discord for more information about the project: https://discord.gg/y2HUxg3y4P
