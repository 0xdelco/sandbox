# Optimism Prime Vision

## Context

Optimism Prime (OPP) is the first meme token of Optimism. Created by a team of passionate DeFi users, it has been entirely distributed in the fairest way through a Velodrome pool. Only tokens kept by the team were used to bribe the pool and incentivize liquidity for several weeks.
The team also raised 15k from a NFT sale and used that amount to buy and lock 1.2m VELO to ensure a minimal liquidity floor on OPP-ETH through VELO incentives.

## Vision

The main objective is now to become the main Social & Memic hub of Optimism. We want to keep a strong DeFi DNA for the project by building on top of other Optimism protocols a range of gamified DeFi applications that will provide an entertaining experience to users and a strong feeling of cohesion and identity for the broad ecosystem.

The theme chosen by creators of the project, “transformers”, allows a natural connection to DeFi “automation”. Our goal is to leverage this connection to offer a stronger thematic identity to the project and its relationship to Optimism, a chain at the center of DeFi.

## Ecosystem Value Proposition

Optimism Prime was already launched with the goal of becoming an important project of the ecosystem. By becoming a Velodrome whale, the project secured a central position to its meme token. Now the goal is to go one step further by offering gamified services on top of several DeFi projects from the Optimism ecosystem.

Leveraging social networks and the memic aspect of the project, we plan to attract users to the chain and its application. We’ll provide a gamified layer that could be qualified as a “metaverse”, where services from other dApp would be integrated as mini-games, quests, and tournaments. Applications of the ecosystem would be clearly represented, by merging the “transformers” thematic with the colors and branding of each project. This gamified layer would provide entertainment, free advertising and offer unique partnership opportunities between projects. The sentiment of identity that could emerge from such a virtual world can provide a strong feeling of connection between Optimism users and reinforce the “Optimism town” vision that has been pushed by the Optimism DAO. If Optimism is a town, then Optimism Prime would be its “amusement park”.

But entertainment is not the only thing we want to provide. As convinced DeFi users, we also believe in services built through the power of interoperability. While our products will have a memic and funny identity, we also want them to be useful for DeFi users. As such, our members already started working on several protocol ideas that would leverage existing protocols to provide new services for Optimism users. Such products are a single side staking solution built on top of Velodrome, and a fair lottery using OPP and VELO for price pools.

## Our hub: The Optimism Prime Theme Park

The main frontend for Optimism Prime will be presented as a game interface for an amusement park.

This park will be split into cells, where each cell is an attraction that represents a product of our ecosystem. Take Disneyland map as reference for what it could look like:

![](https://printablemapjadi.com/wp-content/uploads/2019/07/disneyland-e280a2-restaurant-map-disney-disneyland-restaurants-printable-disneyland-map-2014.gif)

Having this kind of setup offer us a space to experiment and scale up as we wish. At the beginning only a few cells will have content, and others will be represented "in construction". We can tease some of our future products on these cells with some designs.

The global thematic of the park will be robots & factory, as well as other elements reminding the "transformers" lore.

At the center of the map will be the factory to rebuild Cyberton. This is the ultimate goal of the project and we'll design a quest shared by all players to reach that goal one day.

Every cell will be a product of our ecosystem. It can be a complete custom product build by us, or a simple gamified frontend to other services.

For example, we can have a cell hosting a shop where people can swap OPP, but the UI just interact with a DEX aggregator contract. This can be done for farming VELO, lending OPP on Tarot, doing leveraging farming, ... Even just gamified frontends for DeFi products unrelated to OPP directly can be interesting.
There are several interest in having gamified frontends in our application:
- Practical for users
- Open partnership opportunities
- Gives users a feeling of "cohesion"
- Allow us to expend our metaverse and range of products
- Allow us to put some taxes, and to incentivize our users from a central place
- Contributes to make Optimism Prime a central hub of Optimism

Some cells will host Optimism Prime specific products built by us. Some ideas to be developed are:

- DeFi
  - Single Staking Product & Energon farming system
  - OPP fair lottery (maybe partnership with Pool Together)
  - Leveraged farming optimizer (to rebuild Cybertron faster !)
- NFTs/GameFi:
  - Autobot factory
  - Social quests with transformers thematics
  - Trading competitions on isolated virtual markets (trading for fun)
  - Wars to defend the park against Decepticons using crafted Autobots

The range of ideas is basically infinite with such universe merging DeFi & transformers. At the beginning we will need to focus on specific products and implement them well. It will probably be better to start with gamified frontends, as they will be easier to implement, limit smart contract risks from our side, and allow to start shaping the park. **This could serve as an initial representation of what we want to build for the ecosystem, in order to motivate the allocation of an Optimism grant to our project.**

## Roadmap

- Creation of a funding DAO. The goal of that DAO is to provide initial funding and to build a cash flow to support future developments for the project.
- Increase actions on social networks to gain awareness and traction on our projects (meme art production, regular posting, raids)
- Submission of a proposal to get an Optimism grant. The goal of that grant is to provides incentives to our users and grow our ecosystem.
- Design & Specification of the Optimism Prime Metaverse
- Design & Implementation of a first NFT based gamified DeFi product on top of the Autobot NFT collection & a list of several protocols
- Design & Implementation of the lottery
- Design & Implementation of the single staking product
- Implementation of a frontend for the Optimism Prime Metaverse with integration of our products
